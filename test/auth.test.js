const assert = require('assert')
const redis = require('../src/redis')
const auth = require('../src/auth')

describe('Authentication', function () {
  it('will set access token', async function () {
    const access_token = 'fake.access.token'

    await auth.setTokens(access_token)

    assert.equal(await redis.get('access_token'), access_token)
  })

  it('will set access and refresh tokens', async function () {
    const access_token = 'fake.access.token'
    const refresh_token = 'fake.refresh.token'

    await auth.setTokens(access_token, refresh_token)

    assert.equal(await redis.get('access_token'), access_token)
    assert.equal(await redis.get('refresh_token'), refresh_token)
  })

  it('will get access and refresh tokens', async function () {
    const access_token = 'fake.access.token'
    const refresh_token = 'fake.refresh.token'

    await redis.set('access_token', access_token)
    await redis.set('refresh_token', refresh_token)

    assert.equal(await auth.getAccessToken(), access_token)
    assert.equal(await auth.getRefreshToken(), refresh_token)
  })

  it('will delete access and refresh tokens', async function () {
    const access_token = 'fake.access.token'
    const refresh_token = 'fake.refresh.token'

    await redis.set('access_token', access_token)
    await redis.set('refresh_token', refresh_token)

    await auth.deleteTokens()

    assert.equal(await redis.get('access_token'), null)
    assert.equal(await redis.get('refresh_token'), null)
  })

  it('will throw error if access token is not defined', async function () {
    return auth.getAccessToken().catch((e) => {
      assert.equal(e.message, 'Access token is not available')
      assert.equal(e.response.status, 401)
      assert.equal(e.response.data.code, 401)
      assert.equal(e.response.data.status, 'Unauthorized')
    })
  })
})
