const assert = require('assert')

const cache = require('../src/cache')
const redis = require('../src/redis')

describe('Cache', function () {
  it('will JSONify data from redis', async function () {
    redis.set('cache:some-key', JSON.stringify({ key: 'value' }))

    const data = await cache.get('some-key')
    assert.equal(typeof data, 'object')
    assert.equal(data.key, 'value')
  })

  it('will not parse null values', async function () {
    assert.equal(await cache.get('some-key'), null)
  })

  it('will stringify data to redis', async function () {
    await cache.set('some-key', { key: 'value' })

    const data = await redis.get('cache:some-key')
    assert.equal(typeof data, 'string')
    assert.equal(data, '{"key":"value"}')
  })

  it('will clear all cache keys', async function () {
    for (let i of Array.from({ length: 200 }, (_, i) => i)) {
      await redis.set(`cache:key-${i}`, JSON.stringify({ value: i }))
    }

    let keys
    keys = await redis.keys('cache:*')
    assert.equal(keys.length, 200)

    await cache.clear()

    keys = await redis.keys('cache:*')
    assert.equal(keys.length, 0)
  })
})
