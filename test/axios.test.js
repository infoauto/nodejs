const nock = require('nock')
const assert = require('assert')

const axios = require('../src/axios')
const redis = require('../src/redis')

describe('Axios', function () {
  describe('Authorization', function () {
    it('will set authorization header', async function () {
      const access_token = 'fake.access.token'
      await redis.set('access_token', access_token)

      nock(process.env.BASE_URL, {
        reqheaders: { authorization: `Bearer ${access_token}` },
      })
        .get('/pub/test')
        .reply(200, { message: 'Authenticated' })

      return axios.ia.get('/test').then((response) => {
        assert.equal(response.data.message, 'Authenticated')
      })
    })
  })

  describe('Retry', function () {
    it('will retry 429 responses for IA client', async function () {
      const access_token = 'fake.access.token'
      await redis.set('access_token', access_token)

      nock(process.env.BASE_URL)
        .get('/pub/test')
        .reply(429, { message: 'Too many requests' }, { 'retry-after': 1 })
        .get('/pub/test')
        .reply(200, { message: 'Thanks for waiting' })

      return axios.ia.get('/test').then((response) => {
        assert.equal(response.data.message, 'Thanks for waiting')
      })
    })

    it('will retry 5xx responses for IA client', async function () {
      this.timeout(5000)

      const access_token = 'fake.access.token'
      await redis.set('access_token', access_token)

      nock(process.env.BASE_URL)
        .get('/pub/test')
        .reply(502, { message: 'Bad gateway' })
        .get('/pub/test')
        .reply(503, { message: 'Service Unavailable' })
        .get('/pub/test')
        .reply(200, { message: 'Thanks for waiting' })

      return axios.ia.get('/test').then((response) => {
        assert.equal(response.data.message, 'Thanks for waiting')
      })
    })

    it('will retry 5xx responses for Auth client', async function () {
      this.timeout(5000)

      const fake_access_token = 'fake.access.token'
      const fake_refresh_token = 'fake.refresh.token'

      nock(process.env.BASE_URL)
        .post('/auth/login')
        .reply(502, { message: 'Bad gateway' })
        .post('/auth/login')
        .reply(503, { message: 'Service Unavailable' })
        .post('/auth/login')
        .basicAuth({ user: 'test@test.com', pass: 'password' })
        .reply(200, { access_token: fake_access_token, refresh_token: fake_refresh_token })

      return axios.auth
        .post(
          '/login',
          {},
          {
            auth: {
              username: 'test@test.com',
              password: 'password',
            },
          }
        )
        .then(({ data: { access_token, refresh_token } }) => {
          assert.equal(access_token, fake_access_token)
          assert.equal(refresh_token, fake_refresh_token)
        })
    })
  })
})
