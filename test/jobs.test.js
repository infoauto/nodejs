const nock = require('nock')
const sinon = require('sinon')
const assert = require('assert')
const fs = require('fs')

const cache = require('../src/cache')
const redis = require('../src/redis')
const jobs = require('../src/jobs')
const models = require('../src/models')

describe('Jobs', function () {
  describe('Login', function () {
    it('will save access and refresh tokens in redis', async function () {
      assert.equal(await redis.get('access_token'), null)
      assert.equal(await redis.get('refresh_token'), null)

      const access_token = 'fake.access.token'
      const refresh_token = 'fake.refresh.token'

      nock(process.env.BASE_URL)
        .post('/auth/login')
        .basicAuth({ user: 'test@test.com', pass: 'password' })
        .reply(200, { access_token, refresh_token })

      await jobs.login()

      assert.equal(await redis.get('access_token'), access_token)
      assert.equal(await redis.get('refresh_token'), refresh_token)
    })

    it('will clear access and refresh tokens if there is an error', async function () {
      const access_token = 'fake.access.token'
      const refresh_token = 'fake.refresh.token'

      await redis.set('access_token', access_token)
      await redis.set('refresh_token', refresh_token)

      nock(process.env.BASE_URL)
        .post('/auth/login')
        .reply(403, { status: 'The given credentials are not active' })

      await jobs.login()

      assert.equal(await redis.get('access_token'), null)
      assert.equal(await redis.get('refresh_token'), null)
    })
  })

  describe('Refresh', function () {
    it('will save access token in redis', async function () {
      assert.equal(await redis.get('access_token'), null)

      const access_token = 'fake.access.token'
      const refresh_token = 'fake.refresh.token'
      await redis.set('refresh_token', refresh_token)

      nock(process.env.BASE_URL, {
        reqheaders: {
          authorization: `Bearer ${refresh_token}`,
        },
      })
        .post('/auth/refresh')
        .reply(200, { access_token })

      await jobs.refresh()

      assert.equal(await redis.get('access_token'), access_token)
    })

    it('will call login if there is an error', async function () {
      assert.equal(await redis.get('access_token'), null)

      const access_token = 'fake.access.token'
      const refresh_token = 'fake.refresh.token'
      await redis.set('refresh_token', refresh_token)

      nock(process.env.BASE_URL)
        .post('/auth/login')
        .basicAuth({ user: 'test@test.com', pass: 'password' })
        .reply(200, { access_token, refresh_token })

      nock(process.env.BASE_URL, {
        reqheaders: {
          authorization: `Bearer ${refresh_token}`,
        },
      })
        .post('/auth/refresh')
        .reply(422, { status: 'Invalid signature' })

      await jobs.refresh()

      assert.equal(await redis.get('access_token'), access_token)
    })

    it('will call login if there is no refresh token available', async function () {
      const access_token = 'fake.access.token'
      const refresh_token = 'fake.refresh.token'

      nock(process.env.BASE_URL)
        .post('/auth/login')
        .basicAuth({ user: 'test@test.com', pass: 'password' })
        .reply(200, { access_token, refresh_token })

      await jobs.refresh()

      assert.equal(await redis.get('access_token'), access_token)
      assert.equal(await redis.get('refresh_token'), refresh_token)
    })
  })

  describe('Update', function () {
    it('will not update data if updated_at did not change', async function () {
      await redis.set('access_token', 'fake.access.token')
      await redis.set('updated_at', '2023-03-04T12:00:00')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      await models.Model.insertMany([{ codia: 10001 }, { codia: 10005 }, { codia: 180594 }])

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      await jobs.updateData()

      const query = await models.Model.find({ complete: true })
      assert.equal(query.length, 0)
      assert(!fake_cache_clear.called)
    })

    it('will update data if updated_at did change', async function () {
      await redis.set('access_token', 'fake.access.token')
      await redis.set('updated_at', '2023-02-04T12:00:00')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      nock(process.env.BASE_URL)
        .get('/pub/brands/download/')
        .replyWithFile(200, __dirname + '/responses/brands.json')

      nock(process.env.BASE_URL)
        .get('/pub/features/')
        .replyWithFile(200, __dirname + '/responses/features.json')

      nock(process.env.BASE_URL)
        .get('/auth/whoami')
        .reply(200, { roles: [{ name: 'Lotes' }] })

      await jobs.updateData()

      const brandsQuery = await models.Brand.find()
      assert.equal(brandsQuery.length, 12)

      const featuresQuery = await models.Feature.find()
      assert.equal(featuresQuery.length, 83)

      assert(fake_cache_clear.calledOnce)
      assert.equal(await redis.get('updated_at'), '2023-03-04T12:00:00')
    })

    it('will update data if updated_at is undefined', async function () {
      await redis.set('access_token', 'fake.access.token')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      nock(process.env.BASE_URL)
        .get('/pub/brands/download/')
        .replyWithFile(200, __dirname + '/responses/brands.json')

      nock(process.env.BASE_URL)
        .get('/pub/features/')
        .replyWithFile(200, __dirname + '/responses/features.json')

      nock(process.env.BASE_URL)
        .get('/auth/whoami')
        .reply(200, { roles: [{ name: 'Lotes' }] })

      await jobs.updateData()

      const brandsQuery = await models.Brand.find()
      assert.equal(brandsQuery.length, 12)

      const featuresQuery = await models.Feature.find()
      assert.equal(featuresQuery.length, 83)

      assert(fake_cache_clear.calledOnce)
      assert.equal(await redis.get('updated_at'), '2023-03-04T12:00:00')
    })

    it('will complete models data with brand, group and features', async function () {
      await redis.set('access_token', 'fake.access.token')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      await models.Model.insertMany([
        { codia: 10001 },
        { codia: 10003 },
        { codia: 180594 },
        { codia: 180595 },
        { codia: 180718 },
      ])

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      nock(process.env.BASE_URL)
        .get('/pub/brands/download/')
        .replyWithFile(200, __dirname + '/responses/brands.json')

      nock(process.env.BASE_URL)
        .get('/pub/features/')
        .replyWithFile(200, __dirname + '/responses/features.json')

      nock(process.env.BASE_URL)
        .get('/auth/whoami')
        .reply(200, { roles: [{ name: 'Lotes' }] })

      nock(process.env.BASE_URL)
        .post('/pub/batch/', { batch: [10001, 10003, 180594, 180595, 180718] })
        .replyWithFile(200, __dirname + '/responses/batch.json')

      await jobs.updateData()

      const brandsQuery = await models.Brand.find()
      assert.equal(brandsQuery.length, 12)

      const featuresQuery = await models.Feature.find()
      assert.equal(featuresQuery.length, 83)

      const modelsQuery = await models.Model.find({ complete: true })
      assert.equal(modelsQuery.length, 5)

      const model_180594 = await models.Model.findOne(
        { codia: 180594 },
        { 'features._id': 0 }
      ).lean()

      assert(model_180594.complete)
      assert.equal(model_180594.brand.name, 'FORD')
      assert.equal(model_180594.group.name, 'FIESTA')

      const features_180594 = JSON.parse(
        fs.readFileSync(__dirname + '/responses/features_180594.json', 'utf8')
      )

      assert.equal(model_180594.features.length, features_180594.length)
      assert(
        model_180594.features
          .map((e, i) => [e, features_180594[i]])
          .every(
            ([o1, o2]) =>
              Object.keys(o1).length === Object.keys(o2).length &&
              Object.keys(o1).every((p) => o1[p] === o2[p])
          )
      )

      assert(fake_cache_clear.calledOnce)
      assert.equal(await redis.get('updated_at'), '2023-03-04T12:00:00')
    })

    it('will drop models collection if user not authorized for batch service', async function () {
      await redis.set('access_token', 'fake.access.token')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      await models.Model.insertMany([
        { codia: 10001 },
        { codia: 10003 },
        { codia: 180594 },
        { codia: 180595 },
        { codia: 180718 },
      ])

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      nock(process.env.BASE_URL)
        .get('/pub/brands/download/')
        .replyWithFile(200, __dirname + '/responses/brands.json')

      nock(process.env.BASE_URL)
        .get('/pub/features/')
        .replyWithFile(200, __dirname + '/responses/features.json')

      nock(process.env.BASE_URL)
        .get('/auth/whoami')
        .reply(200, { roles: [{ name: 'Modelos' }] })

      await jobs.updateData()

      const brandsQuery = await models.Brand.find()
      assert.equal(brandsQuery.length, 12)

      const featuresQuery = await models.Feature.find()
      assert.equal(featuresQuery.length, 83)

      const modelsQuery = await models.Model.find()
      assert.equal(modelsQuery.length, 0)

      assert(fake_cache_clear.calledOnce)
      assert.equal(await redis.get('updated_at'), '2023-03-04T12:00:00')
    })

    it('will clear models data in rejected batches', async function () {
      this.timeout(3000)

      await redis.set('access_token', 'fake.access.token')

      const fake_cache_clear = sinon.fake.resolves()
      sinon.replace(cache, 'clear', fake_cache_clear)

      await models.Model.insertMany([
        { codia: 10001 },
        { codia: 10003 },
        { codia: 180594 },
        { codia: 180595 },
        { codia: 180718 },
        { codia: 20001, prices: [{ year: 2002, price: 1010 }] },
        { codia: 20003, features: [{ id: 1, value: 100 }] },
        { codia: 20005, list_price: 1030 },
      ])

      nock(process.env.BASE_URL)
        .get('/pub/datetime')
        .reply(200, { updated_at: '2023-03-04T12:00:00' })

      nock(process.env.BASE_URL)
        .get('/pub/brands/download/')
        .replyWithFile(200, __dirname + '/responses/brands.json')

      nock(process.env.BASE_URL)
        .get('/pub/features/')
        .replyWithFile(200, __dirname + '/responses/features.json')

      nock(process.env.BASE_URL)
        .get('/auth/whoami')
        .reply(200, { roles: [{ name: 'Lotes' }] })

      nock(process.env.BASE_URL)
        .post('/pub/batch/', { batch: [10001, 10003, 180594, 180595, 180718] })
        .replyWithFile(200, __dirname + '/responses/batch.json')
        .post('/pub/batch/', { batch: [20001, 20003, 20005] })
        .reply(400)

      await jobs.updateData({ batchSize: 5 })

      const brandsQuery = await models.Brand.find()
      assert.equal(brandsQuery.length, 12)

      const featuresQuery = await models.Feature.find()
      assert.equal(featuresQuery.length, 83)

      const modelsQueryCompl = await models.Model.find({ complete: true })
      assert.equal(modelsQueryCompl.length, 5)

      const modelsQueryRej = await models.Model.find({ rejected: true })
      assert.equal(modelsQueryRej.length, 3)

      const model_20001 = await models.Model.findOne({ codia: 20001 }).lean()
      assert.equal(model_20001.prices.length, 0)

      const model_20003 = await models.Model.findOne({ codia: 20003 }).lean()
      assert.equal(model_20003.features.length, 0)

      const model_20005 = await models.Model.findOne({ codia: 20005 }).lean()
      assert.equal(model_20005.list_price, null)

      assert(fake_cache_clear.calledOnce)
      assert.equal(await redis.get('updated_at'), '2023-03-04T12:00:00')
    })
  })
})
