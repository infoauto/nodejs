const assert = require('assert')

const hashids = require('../src/hashids')

describe('Hashids', function () {
  it('will encode a single object', async function () {
    const data = hashids.codiaEncoder({
      codia: 10001,
      as_codia: 19701,
      r_codia: 9999999,
      description: 'Testing encoder',
    })

    assert.equal(data.codia, hashids.encode(10001))
    assert.equal(data.as_codia, hashids.encode(19701))
    assert.equal(data.r_codia, hashids.encode(9999999))
    assert.equal(data.description, 'Testing encoder')
  })

  it('will encode an array of objects', async function () {
    const data = hashids.codiaEncoder([
      {
        codia: 10001,
        as_codia: 19701,
        r_codia: 9999999,
        description: 'Testing encoder 1',
      },
      {
        codia: 10005,
        as_codia: 19703,
        r_codia: null,
        description: 'Testing encoder 2',
      },
    ])

    assert.equal(data[0].codia, hashids.encode(10001))
    assert.equal(data[0].as_codia, hashids.encode(19701))
    assert.equal(data[0].r_codia, hashids.encode(9999999))
    assert.equal(data[0].description, 'Testing encoder 1')

    assert.equal(data[1].codia, hashids.encode(10005))
    assert.equal(data[1].as_codia, hashids.encode(19703))
    assert.equal(data[1].r_codia, hashids.encode(null))
    assert.equal(data[1].description, 'Testing encoder 2')
  })

  it('will not encode boolean as_codia', async function () {
    const data = hashids.codiaEncoder([
      {
        codia: 10001,
        as_codia: false,
        r_codia: 9999999,
        description: 'Testing encoder 1',
      },
      {
        codia: 10005,
        as_codia: true,
        r_codia: null,
        description: 'Testing encoder 2',
      },
    ])

    assert.equal(data[0].codia, hashids.encode(10001))
    assert.equal(data[0].as_codia, false)
    assert.equal(data[0].r_codia, hashids.encode(9999999))
    assert.equal(data[0].description, 'Testing encoder 1')

    assert.equal(data[1].codia, hashids.encode(10005))
    assert.equal(data[1].as_codia, true)
    assert.equal(data[1].r_codia, hashids.encode(null))
    assert.equal(data[1].description, 'Testing encoder 2')
  })
})
