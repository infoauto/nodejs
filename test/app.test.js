const fs = require('fs')
const request = require('supertest')
const assert = require('assert')
const nock = require('nock')

const app = require('../src/app')
const models = require('../src/models')
const redis = require('../src/redis')
const cache = require('../src/cache')
const hashids = require('../src/hashids')

describe('Express', function () {
  beforeEach(async function () {
    await redis.set('access_token', 'fake.access.token')
  })

  describe('Brands', function () {
    it('will get brands from MongoDB', async function () {
      const brands = JSON.parse(fs.readFileSync(__dirname + '/responses/brands.json', 'utf8'))
      await models.Brand.insertMany(brands)

      return request(app)
        .get('/brands/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 12)
        })
    })

    it('will get brands from Redis cache', async function () {
      const cacheKey = Buffer.from('/brands/').toString('base64')
      cache.set(cacheKey, [
        { id: 1, name: 'ACURA' },
        { id: 18, name: 'FORD' },
      ])

      return request(app)
        .get('/brands/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 2)
        })
    })

    it('will get brand groups from MongoDB', async function () {
      const brands = JSON.parse(fs.readFileSync(__dirname + '/responses/brands.json', 'utf8'))
      await models.Brand.insertMany(brands)

      return request(app)
        .get('/brands/36/groups/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 4)
        })
    })

    it('will get brand groups from Redis cache', async function () {
      const cacheKey = Buffer.from('/brands/36/groups/').toString('base64')
      cache.set(cacheKey, [
        { id: 1, name: 'CLIO' },
        { id: 2, name: 'KOLEOS' },
        { id: 3, name: 'KWID' },
        { id: 4, name: 'MASTER' },
      ])

      return request(app)
        .get('/brands/36/groups/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 4)
        })
    })

    it('will not find groups not in MongoDB', async function () {
      const brands = JSON.parse(fs.readFileSync(__dirname + '/responses/brands.json', 'utf8'))
      await models.Brand.insertMany(brands)

      const b10 = await models.Brand.findOne({ id: 10 })
      assert.equal(b10, null)

      return request(app).get('/brands/10/groups/').expect(404)
    })

    it('will not find groups for invalid brand id', async function () {
      return request(app).get('/brands/nan/groups/').expect(404)
    })
  })

  describe('Features', function () {
    it('will get features from MongoDB', async function () {
      const features = JSON.parse(fs.readFileSync(__dirname + '/responses/features.json', 'utf8'))
      await models.Feature.insertMany(features)

      return request(app)
        .get('/features/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 83)
        })
    })

    it('will get features from Redis cache', async function () {
      const cacheKey = Buffer.from('/features/').toString('base64')
      cache.set(cacheKey, [
        { id: 1, description: 'Peso' },
        { id: 2, description: 'Largo' },
      ])

      return request(app)
        .get('/features/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 2)
        })
    })

    it('will get feature choices from MongoDB', async function () {
      const features = JSON.parse(fs.readFileSync(__dirname + '/responses/features.json', 'utf8'))
      await models.Feature.insertMany(features)

      const f1 = await models.Feature.findOne({ id: 1 })
      assert.equal(f1.type, 'choice')

      return request(app)
        .get('/features/1/choices/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 7)
        })
    })

    it('will get feature choices from Redis cache', async function () {
      const cacheKey = Buffer.from('/features/1/choices/').toString('base64')
      cache.set(cacheKey, [
        { id: 1, description: 'AL', long_description: 'Alarma' },
        { id: 2, description: 'CA', long_description: 'Camara' },
        { id: 3, description: 'NO', long_description: 'No tiene sensor de estacionamiento' },
      ])
      return request(app)
        .get('/features/1/choices/')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 3)
        })
    })

    it('will not find feature choices not in MongoDB', async function () {
      const features = JSON.parse(fs.readFileSync(__dirname + '/responses/features.json', 'utf8'))
      await models.Feature.insertMany(features)

      const f40 = await models.Feature.findOne({ id: 40 })
      assert.notEqual(f40.type, 'choice')

      return request(app).get('/features/40/choices/').expect(404)
    })

    it('will not find choices for invalid feature id', async function () {
      return request(app).get('/features/nan/choices/').expect(404)
    })
  })

  describe('Models', function () {
    it('will call remote search and set pagination header', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/search/')
        .replyWithFile(200, __dirname + '/responses/search.json', {
          'X-Pagination': JSON.stringify({
            total: 150,
            total_pages: 15,
            first_page: 1,
            last_page: 15,
            page: 1,
            next_page: 2,
            page_size: 10,
          }),
        })

      return request(app)
        .get('/search/')
        .expect(200)
        .expect((res) =>
          assert.deepStrictEqual(JSON.parse(res.headers['x-pagination']), {
            total: 150,
            total_pages: 15,
            first_page: 1,
            last_page: 15,
            page: 1,
            next_page: 2,
            page_size: 10,
          })
        )
        .then((response) => {
          assert.equal(response.body.length, 10)
        })
    })

    it('will call remote search with query params', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/search/')
        .query({ page_size: 20 })
        .replyWithFile(200, __dirname + '/responses/search_page_size_20.json')

      return request(app)
        .get('/search/?page_size=20')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 20)
        })
    })

    it('will call remote search and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/search/')
        .query({ page_size: 'Nan' })
        .reply(422, {
          code: 422,
          errors: {
            query: {
              page_size: ['Not a valid integer.'],
            },
          },
          status: 'Unprocessable Entity',
        })

      return request(app)
        .get('/search/?page_size=Nan')
        .expect(422)
        .then((response) => {
          assert.equal(response.body.errors.query.page_size, 'Not a valid integer.')
        })
    })

    it('will call remote brand models and set pagination header', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/models/')
        .replyWithFile(200, __dirname + '/responses/brand_models.json', {
          'X-Pagination': JSON.stringify({
            total: 7,
            total_pages: 1,
            first_page: 1,
            last_page: 1,
            page: 1,
            page_size: 10,
          }),
        })

      return request(app)
        .get('/brands/1/models/')
        .expect(200)
        .expect((res) =>
          assert.deepStrictEqual(JSON.parse(res.headers['x-pagination']), {
            total: 7,
            total_pages: 1,
            first_page: 1,
            last_page: 1,
            page: 1,
            page_size: 10,
          })
        )
        .then((response) => {
          assert.equal(response.body.length, 7)
        })
    })

    it('will call remote brand models with query params', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/models/')
        .query({ page_size: 5 })
        .replyWithFile(200, __dirname + '/responses/brand_models_page_size_5.json')

      return request(app)
        .get('/brands/1/models/?page_size=5')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 5)
        })
    })

    it('will call remote brand models and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/models/')
        .query({ page_size: 'Nan' })
        .reply(422, {
          code: 422,
          errors: {
            query: {
              page_size: ['Not a valid integer.'],
            },
          },
          status: 'Unprocessable Entity',
        })

      return request(app)
        .get('/brands/1/models/?page_size=Nan')
        .expect(422)
        .then((response) => {
          assert.equal(response.body.errors.query.page_size, 'Not a valid integer.')
        })
    })

    it('will call remote brand group models and set pagination header', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/groups/1/models/')
        .replyWithFile(200, __dirname + '/responses/brand_group_models.json', {
          'X-Pagination': JSON.stringify({
            total: 4,
            total_pages: 1,
            first_page: 1,
            last_page: 1,
            page: 1,
            page_size: 10,
          }),
        })

      return request(app)
        .get('/brands/1/groups/1/models/')
        .expect(200)
        .expect((res) =>
          assert.deepStrictEqual(JSON.parse(res.headers['x-pagination']), {
            total: 4,
            total_pages: 1,
            first_page: 1,
            last_page: 1,
            page: 1,
            page_size: 10,
          })
        )
        .then((response) => {
          assert.equal(response.body.length, 4)
        })
    })

    it('will call remote brand group models with query params', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/groups/1/models/')
        .query({ page_size: 2 })
        .replyWithFile(200, __dirname + '/responses/brand_group_models_page_size_2.json')

      return request(app)
        .get('/brands/1/groups/1/models/?page_size=2')
        .expect(200)
        .then((response) => {
          assert.equal(response.body.length, 2)
        })
    })

    it('will call remote brand group models and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/brands/1/groups/1/models/')
        .query({ page_size: 'Nan' })
        .reply(422, {
          code: 422,
          errors: {
            query: {
              page_size: ['Not a valid integer.'],
            },
          },
          status: 'Unprocessable Entity',
        })

      return request(app)
        .get('/brands/1/groups/1/models/?page_size=Nan')
        .expect(422)
        .then((response) => {
          assert.equal(response.body.errors.query.page_size, 'Not a valid integer.')
        })
    })

    it('will call remote model (with as_codia) details and save them in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/10001')
        .replyWithFile(200, __dirname + '/responses/model_10001.json')

      nock(process.env.BASE_URL)
        .get('/pub/models/10001/prices/')
        .reply(200, [{ year: 2000, price: 1040 }])

      nock(process.env.BASE_URL)
        .get('/pub/models/10001/features/')
        .reply(200, [{ id: 1, description: 'Peso', value: 1000 }])

      nock(process.env.BASE_URL).get('/pub/models/10001/as_codia').reply(200, { codia: 190171 })

      return request(app)
        .get(`/models/${hashids.encode(10001)}`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.description, 'INTEGRA LS SM')
          assert.equal(response.body.codia, hashids.encode(10001))
          assert.equal(response.body.as_codia, hashids.encode(190171))
          assert.equal(response.body.prices[0].price, 1040)
          assert.equal(response.body.features[0].value, 1000)

          assert.notEqual(await models.Model.findOne({ codia: 10001, complete: true }), null)
        })
    })

    it('will call remote model (without as_codia) details and save them in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/190171')
        .replyWithFile(200, __dirname + '/responses/model_190171.json')

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/prices/')
        .reply(200, [{ year: 2000, price: 1040 }])

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/list_price')
        .reply(200, { list_price: 1240 })

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/features/')
        .reply(200, [{ id: 1, description: 'Peso', value: 1000 }])

      return request(app)
        .get(`/models/${hashids.encode(190171)}`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.description, 'CIVIC EXL 2.0 L/17')
          assert.equal(response.body.codia, hashids.encode(190171))
          assert.equal(response.body.as_codia, '')
          assert.equal(response.body.prices[0].price, 1040)
          assert.equal(response.body.list_price, 1240)
          assert.equal(response.body.features[0].value, 1000)

          assert.notEqual(await models.Model.findOne({ codia: 190171, complete: true }), null)
        })
    })

    it('will call remote model details and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/10001')
        .reply(404, { code: 404, status: 'Not Found' })

      return request(app)
        .get(`/models/${hashids.encode(10001)}`)
        .expect(404)
        .then(async (response) => {
          assert.equal(response.body.status, 'Not Found')
        })
    })

    it('will find complete model details in MongoDB', async function () {
      const model = JSON.parse(
        fs.readFileSync(__dirname + '/responses/model_630035_complete.json', 'utf8')
      )
      await models.Model.create({ ...model, complete: true })

      return request(app)
        .get(`/models/${hashids.encode(630035)}`)
        .expect(200)
        .then((response) => {
          assert.equal(response.body.brand.name, 'AGRALE')
          assert.equal(response.body.group.name, 'AGRALE')
          assert.equal(response.body.description, '14000 S 4x2')
          assert.equal(response.body.as_codia, '')
          assert.equal(response.body.prices.length, 7)
          assert.equal(response.body.list_price, 4950)
          assert.equal(response.body.features.length, 69)
          assert(!('complete' in response.body))
          assert(!('rejected' in response.body))
        })
    })

    it('will call remote assimilated model details and save them in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/10001/as_codia')
        .replyWithFile(200, __dirname + '/responses/as_model_10001.json')

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/prices/')
        .reply(200, [
          { year: 2020, price: 1000 },
          { year: 2021, price: 1100 },
          { year: 2022, price: 1200 },
        ])

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/features/')
        .reply(200, [{ id: 1, description: 'Peso', value: 1000 }])

      nock(process.env.BASE_URL)
        .get('/pub/models/190171/list_price')
        .reply(200, { list_price: 2550 })

      return request(app)
        .get(`/models/${hashids.encode(10001)}/as_codia`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.codia, hashids.encode(190171))
          assert.equal(response.body.description, 'CIVIC EXL 2.0 L/17')
          assert.equal(response.body.list_price, 2550)
          assert.equal(response.body.prices.length, 3)
          assert.equal(response.body.features[0].value, 1000)

          assert.notEqual(await models.Model.findOne({ codia: 190171, complete: true }), null)
        })
    })

    it('will call remote assimilated model details and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/190171/as_codia')
        .reply(404, { code: 404, status: 'Not Found' })

      return request(app)
        .get(`/models/${hashids.encode(190171)}/as_codia`)
        .expect(404)
        .then(async (response) => {
          assert.equal(response.body.status, 'Not Found')
        })
    })

    it('will find assimilated model details in MongoDB', async function () {
      const model_630030 = JSON.parse(
        fs.readFileSync(__dirname + '/responses/model_630030_complete.json', 'utf8')
      )
      const model_630035 = JSON.parse(
        fs.readFileSync(__dirname + '/responses/model_630035_complete.json', 'utf8')
      )
      await models.Model.insertMany([
        { ...model_630030, complete: true },
        { ...model_630035, complete: true },
      ])

      return request(app)
        .get(`/models/${hashids.encode(630030)}/as_codia`)
        .expect(200)
        .then((response) => {
          assert.equal(response.body.brand.name, 'AGRALE')
          assert.equal(response.body.group.name, 'AGRALE')
          assert.equal(response.body.description, '14000 S 4x2')
          assert.equal(response.body.codia, hashids.encode(630035))
          assert.equal(response.body.as_codia, '')
          assert.equal(response.body.prices.length, 7)
          assert.equal(response.body.list_price, 4950)
          assert.equal(response.body.features.length, 69)
          assert(!('complete' in response.body))
          assert(!('rejected' in response.body))
        })
    })

    it('will call remote assimilated model if not found in MongoDB', async function () {
      const model_630030 = JSON.parse(
        fs.readFileSync(__dirname + '/responses/model_630030_complete.json', 'utf8')
      )
      await models.Model.create({ ...model_630030, complete: true })

      nock(process.env.BASE_URL)
        .get('/pub/models/630030/as_codia')
        .reply(200, {
          codia: '630035',
          description: '14000 S 4x2',
          brand: { id: 63, name: 'AGRALE', summary: '' },
          group: { id: 4, name: 'AGRALE', summary: '' },
        })

      nock(process.env.BASE_URL)
        .get('/pub/models/630035/prices/')
        .reply(200, [
          { year: 2020, price: 1000 },
          { year: 2021, price: 1100 },
          { year: 2022, price: 1200 },
        ])

      nock(process.env.BASE_URL)
        .get('/pub/models/630035/features/')
        .reply(200, [{ id: 1, description: 'Peso', value: 1000 }])

      nock(process.env.BASE_URL)
        .get('/pub/models/630035/list_price')
        .reply(200, { list_price: 2550 })

      return request(app)
        .get(`/models/${hashids.encode(630030)}/as_codia`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.brand.name, 'AGRALE')
          assert.equal(response.body.group.name, 'AGRALE')
          assert.equal(response.body.description, '14000 S 4x2')
          assert.equal(response.body.codia, hashids.encode(630035))
          assert.equal(response.body.list_price, 2550)
          assert.equal(response.body.prices.length, 3)
          assert.equal(response.body.features[0].value, 1000)

          assert.notEqual(await models.Model.findOne({ codia: 630035, complete: true }), null)
        })
    })

    it('will call remote model prices and save them in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/10001/prices/')
        .reply(200, [
          { year: 2000, price: 1040 },
          { year: 2001, price: 1150 },
        ])

      return request(app)
        .get(`/models/${hashids.encode(10001)}/prices/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 2)

          const model = await models.Model.findOne({ codia: 10001 })
          assert.equal(model.prices.length, 2)
        })
    })

    it('will call remote model prices and update them in MongoDB', async function () {
      await models.Model.create({
        codia: 10001,
        list_price: 4950,
      })

      nock(process.env.BASE_URL)
        .get('/pub/models/10001/prices/')
        .reply(200, [
          { year: 2000, price: 1040 },
          { year: 2001, price: 1150 },
        ])

      return request(app)
        .get(`/models/${hashids.encode(10001)}/prices/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 2)

          const model = await models.Model.findOne({ codia: 10001 }).lean()
          assert.equal(model.prices.length, 2)
          assert.equal(model.list_price, 4950)
        })
    })

    it('will find model prices in MongoDB', async function () {
      await models.Model.create({
        codia: 10001,
        prices: [
          { year: 2000, price: 1040 },
          { year: 2001, price: 1150 },
        ],
      })

      return request(app)
        .get(`/models/${hashids.encode(10001)}/prices/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 2)
        })
    })

    it('will call remote model list price and save it in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/630035/list_price')
        .reply(200, { list_price: 4950 })

      return request(app)
        .get(`/models/${hashids.encode(630035)}/list_price`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.list_price, 4950)

          const model = await models.Model.findOne({ codia: 630035 })
          assert.equal(model.list_price, 4950)
        })
    })

    it('will call remote model list price and update it in MongoDB', async function () {
      await models.Model.create({
        codia: 630035,
        prices: [
          { year: 2020, price: 1040 },
          { year: 2021, price: 1150 },
        ],
      })

      nock(process.env.BASE_URL)
        .get('/pub/models/630035/list_price')
        .reply(200, { list_price: 4950 })

      return request(app)
        .get(`/models/${hashids.encode(630035)}/list_price`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.list_price, 4950)

          const model = await models.Model.findOne({ codia: 630035 })
          assert.equal(model.list_price, 4950)
          assert.equal(model.prices.length, 2)
        })
    })

    it('will call remote model list price and return error responses', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/10001/list_price')
        .reply(404, { code: 404, status: 'Not Found' })

      return request(app)
        .get(`/models/${hashids.encode(10001)}/list_price`)
        .expect(404)
        .then(async (response) => {
          assert.equal(response.body.status, 'Not Found')
        })
    })

    it('will find model list price in MongoDB', async function () {
      await models.Model.create({
        codia: 630035,
        list_price: 4950,
      })

      return request(app)
        .get(`/models/${hashids.encode(630035)}/list_price`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.list_price, 4950)
        })
    })

    it('will call remote model features and save them in MongoDB', async function () {
      nock(process.env.BASE_URL)
        .get('/pub/models/180594/features/')
        .replyWithFile(200, __dirname + '/responses/features_180594.json')

      return request(app)
        .get(`/models/${hashids.encode(180594)}/features/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 81)

          const model = await models.Model.findOne({ codia: 180594 })
          assert.equal(model.features.length, 81)
        })
    })

    it('will call remote model features and update it in MongoDB', async function () {
      await models.Model.create({
        codia: 180594,
        prices: [
          { year: 2020, price: 1040 },
          { year: 2021, price: 1150 },
        ],
      })

      nock(process.env.BASE_URL)
        .get('/pub/models/180594/features/')
        .replyWithFile(200, __dirname + '/responses/features_180594.json')

      return request(app)
        .get(`/models/${hashids.encode(180594)}/features/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 81)

          const model = await models.Model.findOne({ codia: 180594 })
          assert.equal(model.features.length, 81)
          assert.equal(model.prices.length, 2)
        })
    })

    it('will find model features in MongoDB', async function () {
      await models.Model.create({
        codia: 630035,
        features: [
          { id: 1, description: 'Peso', value: 1400 },
          { id: 2, description: 'Largo', value: 4.5 },
        ],
      })

      return request(app)
        .get(`/models/${hashids.encode(630035)}/features/`)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.length, 2)
        })
    })
  })

  describe('Swagger UI', function () {
    it('will serve swagger UI', async function () {
      return request(app).get('/swagger-ui/').expect(200)
    })
  })
})
