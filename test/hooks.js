require('dotenv-safe').config()

const Mongoose = require('mongoose')
const mongodb = require('mongodb-memory-server')
const sinon = require('sinon')
const nock = require('nock')

const redis = require('../src/redis')
const mongoose = require('../src/mongoose')
const axios = require('../src/axios')

let mongod
let envCopy

exports.mochaHooks = {
  beforeAll: async function () {
    envCopy = { ...process.env }

    mongod = await mongodb.MongoMemoryReplSet.create({ replSet: { count: 2 } })

    process.env.MONGODB_URI = mongod.getUri('test-db')
    process.env.BASE_URL = 'http://test-infoauto-svc.com.ar'
    process.env.USERNAME = 'test@test.com'
    process.env.PASSWORD = 'password'
    process.env.SALT_HASHIDS = 'secret'

    axios.ia.defaults.baseURL = process.env.BASE_URL + '/pub'
    axios.auth.defaults.baseURL = process.env.BASE_URL + '/auth'

    return mongoose.connect()
  },

  afterAll: function () {
    process.env = envCopy

    redis.quit()
    mongoose.disconnect(() => {
      return mongod.stop()
    })
  },

  beforeEach: async function () {
    this.timeout(5000)

    await Mongoose.model('Brand').ensureIndexes()
    await Mongoose.model('Feature').ensureIndexes()
    await Mongoose.model('Model').ensureIndexes()
  },

  afterEach: async function () {
    sinon.restore()
    nock.cleanAll()

    await redis.flushdb()
    const collections = await Mongoose.connection.db.collections()
    for (let collection of collections) {
      await collection.drop({ writeConcern: { w: 1 } })
    }
  },
}
