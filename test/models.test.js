const assert = require('assert')

const models = require('../src/models')

describe('Models', function () {
  describe('Brand', function () {
    it('will find brand groups', async function () {
      await models.Brand.create({
        id: 1,
        name: 'ACURA',
        groups: [
          {
            id: 1,
            list_price: false,
            name: 'INTEGRA',
            prices: true,
            prices_from: 1994,
            prices_to: 1997,
            summary: '',
          },
          {
            id: 2,
            list_price: false,
            name: 'LEGEND',
            prices: true,
            prices_from: 1994,
            prices_to: 1997,
            summary: '',
          },
        ],
      })

      const brand = await models.Brand.findOne({ id: 1 })
      const groups = await brand.findGroups()

      assert.equal(groups.length, 2)
      assert(groups.every((g) => Object.keys(g).length === 7 && !('_id' in g)))
    })
  })

  describe('Feature', function () {
    it('will find features choices', async function () {
      await models.Feature.create({
        choices: [
          { description: 'AL', id: 2, long_description: 'Alarma' },
          { description: 'CA', id: 3, long_description: 'Cámara' },
          { description: 'NO', id: 1, long_description: 'No tiene sensor de estacionamiento' },
        ],
        description: 'Sensor de estacionamiento',
        id: 8,
        type: 'choice',
      })

      const feature = await models.Feature.findOne({ id: 8 })
      const choices = await feature.findChoices()

      assert.equal(choices.length, 3)
      assert(choices.every((c) => Object.keys(c).length === 3 && !('_id' in c)))
    })
  })

  describe('Model', function () {
    it('will find assimilated model if completed', async function () {
      await models.Model.insertMany([
        { codia: 10001, as_codia: 19071 },
        {
          codia: 19071,
          as_codia: null,
          r_codia: null,
          complete: true,
          list_price: 2400,
          prices: [{ year: 2000, price: 1020 }],
          features: [
            {
              category_name: 'Confort',
              description: 'Sensor de estacionamiento',
              id: 8,
              length: 2,
              position: 1,
              type: 'choice',
              value: 'NO',
              value_description: 'No tiene sensor de estacionamiento',
            },
          ],
        },
      ])

      const model_10001 = await models.Model.findOne({ codia: 10001 })
      const model_19071 = await model_10001.findAssimilatedModel()

      assert.equal(model_19071.codia, 19071)
      assert(!('_id' in model_19071.prices[0]))
      assert(!('_id' in model_19071.features[0]))
      assert(!('rejected' in model_19071))
      assert(!('complete' in model_19071))
    })

    it('will not find assimilated model if not completed', async function () {
      await models.Model.insertMany([
        { codia: 10001, as_codia: 19071 },
        {
          codia: 19071,
          list_price: 2400,
          prices: [{ year: 2000, price: 1020 }],
        },
      ])

      const model_10001 = await models.Model.findOne({ codia: 10001 })
      const model_19071 = await model_10001.findAssimilatedModel()

      assert.equal(model_19071, null)
    })
  })
})
