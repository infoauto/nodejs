# InfoAuto / NodeJS

[![pipeline status](https://gitlab.com/infoauto/nodejs/badges/main/pipeline.svg)](https://gitlab.com/infoauto/nodejs/-/commits/main)
[![coverage report](https://gitlab.com/infoauto/nodejs/badges/main/coverage.svg)](https://gitlab.com/infoauto/nodejs/-/commits/main)

## Descripción

Este repositorio contiene la implementación de una integración con el servicio API de InfoAuto. Las principales tecnologías utilizadas son las siguientes:

- [NodeJS](https://nodejs.org/)
- [Express](https://expressjs.com/)
- [Bull](https://optimalbits.github.io/bull/)
- [Axios](https://axios-http.com/)
- [Redis](https://redis.io/)
- [MongoDB](https://www.mongodb.com/)

Las principales características de la integración son:

- Se implementan los servicios de _login_ y _refresh_ para mantener siempre disponible un token de acceso válido
- Se implementa una cache _doble_ usando Redis y MongoDB para minimizar las consultas remotas a la API de InfoAuto
- Se implementa un proceso de actualización masiva de todos los códigos previamente consultados que se ejecuta automáticamente cada vez que se detecta una actualización de la base de datos de InfoAuto
- Se implementa el reenvío automático de consultas rechazadas con códigos de error 429 y 5xx

## Usar con Docker Compose

Crear un archivo llamado `.env` en la raiz del repositorio y definir las siguientes variables de entorno:

```bash
EXPRESS_PORT= # 8080
BASE_URL= # https://demo.api.infoauto.com.ar/cars
USERNAME= # user@company-name.com.ar
PASSWORD= # password
SALT_HASHIDS= # some-secret-word
ALLOWED_ORIGIN= # http://localhost:5173
```

Luego, ejecutar el siguiente comando para iniciar los servicios definidos en el archivo `docker-compose.yml`:

```bash
$ docker compose up
```

Esperar unos segundos para que todos los servicios esten operativos. Los servicios son los siguientes:

- Server: Servidor web basado en Express
- Worker: Worker de tareas basado en Bull
- Redis: Instancia de Redis
- MongoDB1 y MongoDB2: Instancias de MongoDB (ReplicaSet)
- MongoRS: Servicio temporal para configurar el ReplicaSet de MongoDB

**Se debe tener en cuenta que tanto la instancia de Redis como las instancias de MongoDB no están configuradas para ser usadas en un entorno productivo. Se recomienda usar estos servicios únicamente para desarrollo o aplicar las configuraciones que sean necesarias de acuerdo al uso previsto.**

Los _endpoints_ disponibilizados por la integración se pueden consultar ingresando a http://localhost:8080/swagger-ui/ (usar el puerto que corresponda según el valor asignado a la variable de entorno `EXPRESS_PORT`).
