/**
 * @swagger
 * components:
 *   schemas:
 *     Brand:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 18
 *         list_price:
 *           type: boolean
 *           example: true
 *         logo_url:
 *           type: string
 *           format: url
 *           example: https://storage.googleapis.com/images/ford-logo.jpg
 *         name:
 *           type: string
 *           example: FORD
 *         prices:
 *           type: boolean
 *           example: true
 *         prices_from:
 *           type: integer
 *           format: int32
 *           example: 1994
 *         prices_to:
 *           type: integer
 *           format: int32
 *           example: 2022
 *         summary:
 *           type: string
 *           example: Ford Motor Company, más conocida como Ford, es una empresa multinacional de origen estadounidense especializada en la industria automotriz.
 *
 *     NestedBrand:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 18
 *         name:
 *           type: string
 *           example: FORD
 *         summary:
 *           type: string
 *           example: Ford Motor Company, más conocida como Ford, es una empresa multinacional de origen estadounidense especializada en la industria automotriz.
 *
 *     Group:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 16
 *         list_price:
 *           type: boolean
 *           example: false
 *         name:
 *           type: string
 *           example: FIESTA
 *         prices:
 *           type: boolean
 *           example: true
 *         prices_from:
 *           type: integer
 *           format: int32
 *           example: 1994
 *         prices_to:
 *           type: integer
 *           format: int32
 *           example: 2020
 *         summary:
 *           type: string
 *           example: El Ford Fiesta es un automóvil de turismo del segmento B desarrollado por la filial europea de la empresa estadounidense Ford Motor Company.
 *
 *     NestedGroup:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 16
 *         name:
 *           type: string
 *           example: FIESTA
 *         summary:
 *           type: string
 *           example: El Ford Fiesta es un automóvil de turismo del segmento B desarrollado por la filial europea de la empresa estadounidense Ford Motor Company.
 *
 *     Model:
 *       type: object
 *       properties:
 *         as_codia:
 *           type: boolean
 *           example: true
 *         codia:
 *           type: string
 *           example: G9lp2wdJkB
 *         brand:
 *           $ref: '#/components/schemas/NestedBrand'
 *         description:
 *           type: string
 *           example: FIESTA  1.6 4P S (KD)
 *         features:
 *           type: array
 *           items:
 *             type: integer
 *           example: [1,2,3,4,5]
 *         group:
 *           $ref: '#/components/schemas/NestedGroup'
 *         list_price:
 *           type: boolean
 *           example: false
 *         photo_url:
 *           type: string
 *           format: url
 *           example: https://storage.googleapis.com/images/uuid.jpg
 *         prices:
 *           type: boolean
 *           example: true
 *         prices_from:
 *           type: integer
 *           example: 2013
 *         prices_to:
 *           type: integer
 *           example: 2017
 *         position:
 *           type: integer
 *           example: 1
 *         r_codia:
 *           type: string
 *           example: ""
 *         summary:
 *           type: string
 *           example: ""
 *         similarity:
 *           type: number
 *           format: float
 *           example: 0.4
 *
 *     CompleteModel:
 *       type: object
 *       properties:
 *         as_codia:
 *           type: string
 *           example: "p1kVVmvqkO"
 *         codia:
 *           type: string
 *           example: G9lp2wdJkB
 *         brand:
 *           $ref: '#/components/schemas/NestedBrand'
 *         description:
 *           type: string
 *           example: FIESTA  1.6 4P S (KD)
 *         features:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/ModelFeature'
 *         group:
 *           $ref: '#/components/schemas/NestedGroup'
 *         list_price:
 *           type: number
 *           format: float
 *           example: null
 *         photo_url:
 *           type: string
 *           format: url
 *           example: https://storage.googleapis.com/images/uuid.jpg
 *         prices:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Price'
 *           example: [{"price": 493,"year": 2013},{"price": 530,"year": 2014},{"price": 578,"year": 2015},{"price": 610,"year": 2016},{"price": 648,"year": 2017}]
 *         position:
 *           type: integer
 *           example: 1
 *         r_codia:
 *           type: string
 *           example: ""
 *         summary:
 *           type: string
 *           example: ""
 *
 *     ModelFeature:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 1
 *         description:
 *           type: string
 *           example: Combustible
 *         category_name:
 *           type: string
 *           example: Motor y transmisión
 *         length:
 *           type: integer
 *           format: int32
 *           example: 3
 *         decimals:
 *           type: integer
 *           format: int32
 *           example: undefined
 *         position:
 *           type: integer
 *           format: int32
 *           example: 1
 *         type:
 *           type: string
 *           enum: [integer, decimal, boolean, choice]
 *           example: choice
 *         value:
 *           oneOf:
 *             - type: integer
 *             - type: number
 *             - type: boolean
 *             - type: string
 *           example: NAF
 *         value_description:
 *           type: string
 *           example: Nafta
 *
 *     ListPrice:
 *       type: object
 *       properties:
 *         list_price:
 *           type: number
 *           format: float
 *           example: 1400
 *
 *     Price:
 *       type: object
 *       properties:
 *         price:
 *           type: number
 *           format: float
 *           example: 1200
 *         year:
 *           type: integer
 *           format: int32
 *           example: 2010
 *
 *     Feature:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 1
 *         description:
 *           type: string
 *           example: Combustible
 *         category_name:
 *           type: string
 *           example: Motor y transmisión
 *         length:
 *           type: integer
 *           format: int32
 *           example: 3
 *         decimals:
 *           type: integer
 *           format: int32
 *         position:
 *           type: integer
 *           format: int32
 *           example: 1
 *         type:
 *           type: string
 *           enum: [integer, decimal, boolean, choice]
 *           example: choice
 *         choices:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Choice'
 *
 *     Choice:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           format: int32
 *           example: 1
 *         description:
 *           type: string
 *           example: NAF
 *         long_description:
 *           type: string
 *           example: Nafta
 *
 *     PaginationHeader:
 *       type: object
 *       properties:
 *         total:
 *           type: integer
 *           format: int32
 *         page_size:
 *           type: integer
 *           format: int32
 *         total_pages:
 *           type: integer
 *           format: int32
 *         first_page:
 *           type: integer
 *           format: int32
 *         last_page:
 *           type: integer
 *           format: int32
 *         page:
 *           type: integer
 *           format: int32
 *         previous_page:
 *           type: integer
 *           format: int32
 *         next_page:
 *           type: integer
 *           format: int32
 *
 *   responses:
 *     ModelList:
 *       description: OK
 *       headers:
 *         x-pagination:
 *           description: Metadatos de la paginación
 *           schema:
 *             $ref: '#/components/schemas/PaginationHeader'
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/Model'
 *
 *     NotFound:
 *       description: Not Found
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: integer
 *                 format: int32
 *                 example: 404
 *               status:
 *                 type: string
 *                 example: Not Found
 *
 *     UnprocessableEntity:
 *       description: Unprocessable Entity
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: integer
 *                 format: int32
 *                 example: 422
 *               status:
 *                 type: string
 *                 example: Unprocessable Entity
 *               errors:
 *                 type: object
 *                 properties:
 *                   query:
 *                     type: object
 *                     example: {"page": ["Not a valid integer."]}
 *
 * /brands/:
 *   get:
 *     summary: Obtener el listado completo de marcas
 *     description: Devuelve el listado completo de marcas
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Brand'
 *
 * /brands/{brand_id}/models/:
 *   get:
 *     summary: Obtener un listado paginado de modelos dentro de una marca
 *     description: Devuelve un listado paginado de modelos aplicando los filtros provistos como parámetros. Consultar la documentación original de InfoAuto para conocer todos los parámetros disponibles.
 *     parameters:
 *       - in: path
 *         name: brand_id
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *       - in: query
 *         name: query_string
 *         description: Nombre de grupo o descripción de modelo
 *         required: false
 *         schema:
 *           type: string
 *           maxLength: 100
 *           default: ''
 *       - in: query
 *         name: query_mode
 *         description: Modo de búsqueda
 *         required: false
 *         schema:
 *           type: string
 *           default: 'matching'
 *           enum:
 *             - matching
 *             - similarity
 *       - in: query
 *         name: list_price
 *         description: Tiene precio 0km
 *         required: false
 *         schema:
 *           type: boolean
 *           default: null
 *       - in: query
 *         name: price_at
 *         description: Tiene precio usado en un año dado
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: null
 *       - in: query
 *         name: page
 *         description: Página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 1
 *           minimum: 1
 *       - in: query
 *         name: page_size
 *         description: Tamaño de página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 10
 *           minimum: 1
 *           maximum: 100
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         $ref: '#/components/responses/ModelList'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       422:
 *         $ref: '#/components/responses/UnprocessableEntity'
 *
 * /brands/{brand_id}/groups/:
 *   get:
 *     summary: Obtener el listado completo de grupos dentro de una marca
 *     description: Devuelve el listado completo de grupos dado un ID de marca
 *     parameters:
 *       - in: path
 *         name: brand_id
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Group'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *
 * /brands/{brand_id}/groups/{group_id}/models/:
 *   get:
 *     summary: Obtener un listado paginado de modelos dentro de una marca y un grupo
 *     description: Devuelve un listado paginado de modelos aplicando los filtros provistos como parámetros. Consultar la documentación original de InfoAuto para conocer todos los parámetros disponibles.
 *     parameters:
 *       - in: path
 *         name: brand_id
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *       - in: path
 *         name: group_id
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *       - in: query
 *         name: query_string
 *         description: Descripción de modelo
 *         required: false
 *         schema:
 *           type: string
 *           maxLength: 100
 *           default: ''
 *       - in: query
 *         name: query_mode
 *         description: Modo de búsqueda
 *         required: false
 *         schema:
 *           type: string
 *           default: 'matching'
 *           enum:
 *             - matching
 *             - similarity
 *       - in: query
 *         name: list_price
 *         description: Tiene precio 0km
 *         required: false
 *         schema:
 *           type: boolean
 *           default: null
 *       - in: query
 *         name: price_at
 *         description: Tiene precio usado en un año dado
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: null
 *       - in: query
 *         name: page
 *         description: Página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 1
 *           minimum: 1
 *       - in: query
 *         name: page_size
 *         description: Tamaño de página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 10
 *           minimum: 1
 *           maximum: 100
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         $ref: '#/components/responses/ModelList'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       422:
 *         $ref: '#/components/responses/UnprocessableEntity'
 *
 * /search/:
 *   get:
 *     summary: Obtener un listado paginado de modelos
 *     description: Devuelve un listado paginado de modelos aplicando los filtros provistos como parámetros. Consultar la documentación original de InfoAuto para conocer todos los parámetros disponibles.
 *     parameters:
 *       - in: query
 *         name: query_string
 *         description: Nombre de marca, grupo o descripción de modelo
 *         required: false
 *         schema:
 *           type: string
 *           maxLength: 100
 *           default: ''
 *       - in: query
 *         name: query_mode
 *         description: Modo de búsqueda
 *         required: false
 *         schema:
 *           type: string
 *           default: 'matching'
 *           enum:
 *             - matching
 *             - similarity
 *       - in: query
 *         name: list_price
 *         description: Tiene precio 0km
 *         required: false
 *         schema:
 *           type: boolean
 *           default: null
 *       - in: query
 *         name: price_at
 *         description: Tiene precio usado en un año dado
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: null
 *       - in: query
 *         name: page
 *         description: Página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 1
 *           minimum: 1
 *       - in: query
 *         name: page_size
 *         description: Tamaño de página
 *         required: false
 *         schema:
 *           type: integer
 *           format: int32
 *           default: 10
 *           minimum: 1
 *           maximum: 100
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         $ref: '#/components/responses/ModelList'
 *       422:
 *         $ref: '#/components/responses/UnprocessableEntity'
 *
 * /models/{codia}:
 *   get:
 *     summary: Obtener la información completa de un modelo
 *     description: Devuelve la información completa de un modelo dado el CODIA
 *     parameters:
 *       - in: path
 *         name: codia
 *         required: true
 *         schema:
 *           type: string
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CompleteModel'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *
 * /models/{codia}/list_price:
 *   get:
 *     summary: Obtener el precio 0km de un modelo
 *     description: Devuelve el precio 0km de un modelo dado el CODIA
 *     parameters:
 *       - in: path
 *         name: codia
 *         required: true
 *         schema:
 *           type: string
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ListPrice'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *
 * /models/{codia}/prices/:
 *   get:
 *     summary: Obtener los precios usados de un modelo
 *     description: Devuelve los precios usados de un modelo dado el CODIA
 *     parameters:
 *       - in: path
 *         name: codia
 *         required: true
 *         schema:
 *           type: string
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Price'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *
 * /models/{codia}/features/:
 *   get:
 *     summary: Obtener la ficha técnica de un modelo
 *     description: Devuelve la ficha técnica completa de un modelo dado el CODIA
 *     parameters:
 *       - in: path
 *         name: codia
 *         required: true
 *         schema:
 *           type: string
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/ModelFeature'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *
 * /features/:
 *   get:
 *     summary: Obtener todas las características informadas en la ficha técnica
 *     description: Devuelve la especificación de todas las características informadas en la ficha técnica
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Feature'
 *
 * /features/{feature_id}/choices/:
 *   get:
 *     summary: Obtener las opciones de una característica de la ficha técnica
 *     description: Devuelve todas las opciones para una característica de la ficha técnica
 *     parameters:
 *       - in: path
 *         name: feature_id
 *         required: true
 *         schema:
 *           type: integer
 *     tags:
 *       - Servicios
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Choice'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 */
