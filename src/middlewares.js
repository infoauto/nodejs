const cache = require('./cache')
const hashids = require('./hashids')
const models = require('./models')

const hasInfo = (data) =>
  (Array.isArray(data) && data.length) ||
  (typeof data === 'object' &&
    !!data &&
    Object.keys(data).length &&
    Object.keys(data).some((key) => !!data[key]))

const hasCodia = (obj) =>
  Object.prototype.hasOwnProperty.call(obj, 'codia') ||
  Object.prototype.hasOwnProperty.call(obj, 'r_codia') ||
  Object.prototype.hasOwnProperty.call(obj, 'as_codia')

module.exports = {
  cache: async function (req, res, next) {
    const url = new URL(req.protocol + '://' + req.get('host') + req.originalUrl)
    const cacheKey = Buffer.from(url.pathname).toString('base64')

    const cachedResults = await cache.get(cacheKey)
    if (cachedResults) {
      res.send(cachedResults)
      return
    }
    req.cacheKey = cacheKey
    next()
  },
  hashids: async function (req, res, next) {
    req.params.codia = hashids.decode(req.params.hashid)[0]
    next()
  },
  mongodb: ({ projection, returnProperty, filter = {}, lean = true }) =>
    async function (req, res, next) {
      const model = await models.Model.findOne(
        { codia: req.params.codia, ...filter },
        projection
      ).lean(lean)

      if (model !== null) {
        const rawData = returnProperty !== undefined ? model[returnProperty] : model
        const data = hasCodia(rawData) ? hashids.codiaEncoder(rawData) : rawData
        if (hasInfo(data)) {
          await cache.set(req.cacheKey, data)
          res.send(data)
          return
        }
      }
      next()
    },
}
