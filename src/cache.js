const redis = require('./redis')

const CACHE_PREFIX = 'cache:'

module.exports = {
  get: async (key) => {
    const data = await redis.get(CACHE_PREFIX + key)
    return data ? JSON.parse(data) : data
  },
  set: async (key, data) => redis.set(CACHE_PREFIX + key, JSON.stringify(data)),
  clear: async () => {
    const batch = 100
    const jobs = []
    let pipeline = redis.pipeline()
    for await (const keys of redis.scanStream({
      match: CACHE_PREFIX + '*',
      count: batch,
    })) {
      for (const key of keys) {
        pipeline.del(key)
      }
      if (pipeline.length >= batch) {
        jobs.push(pipeline.exec())
        pipeline = redis.pipeline()
      }
    }
    jobs.push(pipeline.exec())
    return await Promise.all(jobs)
  },
}
