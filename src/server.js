require('dotenv-safe').config()

const app = require('./app')
const mongoose = require('./mongoose')
const redis = require('./redis')

mongoose.connect().then(() => {
  const port = process.env.EXPRESS_PORT
  const server = app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
  })

  const cleanup = () => {
    redis.quit(() => {
      console.log('Redis client disconnected.')
      mongoose.disconnect(() => {
        console.log('Mongoose client disconnected.')
        server.close(() => {
          console.log('Server stopped.')
          process.exit()
        })
      })
    })
  }

  process.on('SIGINT', cleanup)
  process.on('SIGTERM', cleanup)
})
