const Hashids = require('hashids')

const hashids = new Hashids(process.env.SALT_HASHIDS, 10)

const encodeCODIA = (rec) => ({
  ...rec,
  codia: hashids.encode(rec.codia),
  r_codia: hashids.encode(rec.r_codia),
  as_codia: typeof rec.as_codia === 'boolean' ? rec.as_codia : hashids.encode(rec.as_codia),
})

hashids.codiaEncoder = (data) => (Array.isArray(data) ? data.map(encodeCODIA) : encodeCODIA(data))

module.exports = hashids
