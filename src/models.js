const mongoose = require('mongoose')

const brandSchema = mongoose.Schema(
  {
    id: { type: Number, unique: true },
    name: String,
    list_price: Boolean,
    logo_url: String,
    prices: Boolean,
    prices_from: Number,
    prices_to: Number,
    summary: String,
    groups: [
      {
        id: Number,
        name: String,
        list_price: Boolean,
        logo_url: String,
        prices: Boolean,
        prices_from: Number,
        prices_to: Number,
        summary: String,
      },
    ],
  },
  {
    methods: {
      async findGroups() {
        return await mongoose
          .model('Brand')
          .aggregate([
            { $match: { id: this.id } },
            { $unwind: '$groups' },
            { $sort: { 'groups.name': 1 } },
            { $project: { 'groups._id': 0 } },
            { $replaceRoot: { newRoot: '$groups' } },
          ])
      },
    },
    versionKey: false,
  }
)

const featureSchema = mongoose.Schema(
  {
    id: { type: Number, unique: true },
    category_name: String,
    description: String,
    type: { type: String },
    position: Number,
    length: Number,
    decimals: Number,
    choices: [
      {
        id: Number,
        description: String,
        long_description: String,
      },
    ],
  },
  {
    methods: {
      async findChoices() {
        return await mongoose
          .model('Feature')
          .aggregate([
            { $match: { id: this.id } },
            { $unwind: '$choices' },
            { $sort: { 'choices.long_description': 1 } },
            { $project: { 'choices._id': 0 } },
            { $replaceRoot: { newRoot: '$choices' } },
          ])
      },
    },
    versionKey: false,
  }
)

const modelSchema = mongoose.Schema(
  {
    complete: { type: Boolean, default: false, index: true },
    rejected: { type: Boolean, default: false, index: true },
    codia: { type: Number, unique: true },
    description: String,
    summary: String,
    r_codia: Number,
    as_codia: Number,
    position: Number,
    list_price: Number,
    photo_url: String,
    brand: {
      id: Number,
      name: String,
      summary: String,
    },
    group: {
      id: Number,
      name: String,
      summary: String,
    },
    prices: [{ year: Number, price: Number }],
    features: [
      {
        id: Number,
        category_name: String,
        description: String,
        type: { type: String },
        position: Number,
        length: Number,
        decimals: Number,
        value: mongoose.Schema.Types.Mixed,
        value_description: String,
      },
    ],
  },
  {
    methods: {
      async findAssimilatedModel(lean = true) {
        return await mongoose
          .model('Model')
          .findOne(
            { codia: this.as_codia, complete: true },
            { _id: 0, 'prices._id': 0, 'features._id': 0, complete: 0, rejected: 0 }
          )
          .lean(lean)
      },
    },
    versionKey: false,
  }
)

module.exports = {
  Brand: mongoose.model('Brand', brandSchema),
  Feature: mongoose.model('Feature', featureSchema),
  Model: mongoose.model('Model', modelSchema),
}
