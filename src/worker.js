require('dotenv-safe').config()

const Queue = require('bull')

const mongoose = require('./mongoose')
const redis = require('./redis')
const jobs = require('./jobs')

const queue = new Queue('Jobs queue', process.env.REDIS_URI)

queue.add('login', {}, { repeat: { cron: '0 */23 * * *' } })
queue.add('refresh', {}, { repeat: { cron: '*/45 * * * *' } })
queue.add('datetime', {}, { repeat: { cron: '0 0 * * *' } })

mongoose.connect().then(async () => {
  await jobs.login()
  await jobs.updateData()

  queue.process('login', jobs.login)
  queue.process('refresh', jobs.refresh)
  queue.process('datetime', jobs.updateData)

  const cleanup = () => {
    redis.quit(() => {
      console.log('Redis client disconnected.')
      mongoose.disconnect(() => {
        console.log('Mongoose client disconnected.')
        queue.close().then(() => {
          console.log('Bull queue closed.')
          process.exit()
        })
      })
    })
  }

  process.on('SIGINT', cleanup)
  process.on('SIGTERM', cleanup)
})
