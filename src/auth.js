const redis = require('./redis')

const ACCESS_TOKEN = 'access_token'
const REFRESH_TOKEN = 'refresh_token'

class AuthenticationError extends Error {
  constructor(message, options) {
    super(message, options)
    this.response = { status: 401, data: { code: 401, status: 'Unauthorized' } }
  }
}

module.exports = {
  setTokens: async (access_token, refresh_token) => {
    await redis.set(ACCESS_TOKEN, access_token)
    if (refresh_token) {
      await redis.set(REFRESH_TOKEN, refresh_token)
    }
  },
  getAccessToken: async () => {
    const token = await redis.get(ACCESS_TOKEN)
    if (!token) {
      throw new AuthenticationError('Access token is not available')
    }
    return token
  },
  getRefreshToken: () => redis.get(REFRESH_TOKEN),
  deleteTokens: async () => {
    await Promise.all([redis.del(ACCESS_TOKEN), redis.del(REFRESH_TOKEN)])
  },
}
