const express = require('express')
const { param } = require('express-validator')
const cors = require('cors')

const axios = require('./axios')
const middlewares = require('./middlewares')
const models = require('./models')
const utils = require('./utils')

const app = express()
app.use(cors({ origin: `${process.env.ALLOWED_ORIGIN}`, exposedHeaders: 'X-Pagination' }))

const NOT_FOUND = { code: 404, status: 'Not Found' }

app.get('/brands/', middlewares.cache, async (req, res) => {
  const { cacheKey } = req

  const data = await models.Brand.find({}, { _id: 0, groups: 0 }).sort({ name: 1 }).lean()
  res.send(await utils.setCache(cacheKey)(data))
})

app.get(
  '/brands/:brandId/groups/',
  middlewares.cache,
  param('brandId').toInt(),
  async (req, res) => {
    const {
      cacheKey,
      params: { brandId },
    } = req

    let brand = null
    if (!isNaN(brandId)) {
      brand = await models.Brand.findOne({ id: brandId })
    }

    if (brand !== null) {
      const data = await brand.findGroups()
      res.send(await utils.setCache(cacheKey)(data))
    } else {
      res.status(404).send(NOT_FOUND)
    }
  }
)

app.get('/search/', (req, res) => {
  const { query } = req
  axios.ia
    .get('/search/', { params: query })
    .then(utils.setPagination(res))
    .then(utils.encodeCODIA)
    .then(utils.sendData(res))
    .catch(utils.sendError(res))
})

app.get('/brands/:brandId/models/', (req, res) => {
  const {
    query,
    params: { brandId },
  } = req

  axios.ia
    .get(`/brands/${brandId}/models/`, { params: query })
    .then(utils.setPagination(res))
    .then(utils.encodeCODIA)
    .then(utils.sendData(res))
    .catch(utils.sendError(res))
})

app.get('/brands/:brandId/groups/:groupId/models/', (req, res) => {
  const {
    query,
    params: { brandId, groupId },
  } = req

  axios.ia
    .get(`/brands/${brandId}/groups/${groupId}/models/`, { params: query })
    .then(utils.setPagination(res))
    .then(utils.encodeCODIA)
    .then(utils.sendData(res))
    .catch(utils.sendError(res))
})

app.get('/features/', middlewares.cache, async (req, res) => {
  const { cacheKey } = req

  const features = await models.Feature.find({}, { _id: 0, 'choices._id': 0 })
    .sort({ category_name: 1, position: 1 })
    .lean()

  res.send(await utils.setCache(cacheKey)(features))
})

app.get(
  '/features/:featureId/choices/',
  middlewares.cache,
  param('featureId').toInt(),
  async (req, res) => {
    const {
      cacheKey,
      params: { featureId },
    } = req

    let feature = null
    if (!isNaN(featureId)) {
      feature = await models.Feature.findOne({ id: featureId, type: 'choice' })
    }

    if (feature !== null) {
      const data = await feature.findChoices()
      res.send(await utils.setCache(cacheKey)(data))
    } else {
      res.status(404).send(NOT_FOUND)
    }
  }
)

app.get(
  '/models/:hashid',
  [
    middlewares.cache,
    middlewares.hashids,
    middlewares.mongodb({
      filter: { complete: true },
      projection: { _id: 0, complete: 0, rejected: 0, 'prices._id': 0, 'features._id': 0 },
    }),
  ],
  async (req, res) => {
    const {
      cacheKey,
      params: { codia },
    } = req

    axios.ia
      .get(`/models/${codia}`)
      .then(async ({ data }) => utils.completeModelData(data))
      .then(utils.createOrUpdateModel(codia))
      .then(({ complete, ...data }) => utils.encodeCODIA({ data }))
      .then(utils.setCache(cacheKey))
      .then(utils.sendData(res))
      .catch(utils.sendError(res))
  }
)

app.get('/models/:hashid/as_codia', [middlewares.cache, middlewares.hashids], async (req, res) => {
  const {
    cacheKey,
    params: { codia },
  } = req

  const model = await models.Model.findOne({ codia, complete: true })
  if (model !== null) {
    const as_model = await model.findAssimilatedModel()
    if (as_model !== null) {
      const data = await utils.setCache(cacheKey)(utils.encodeCODIA({ data: as_model }))
      utils.sendData(res)(data)
      return
    }
  }

  axios.ia
    .get(`/models/${codia}/as_codia`)
    .then(async ({ data }) => {
      const { codia: as_codia } = data

      const completeData = await utils.completeAssimilatedModelData(data)
      await utils.createOrUpdateModel(as_codia)(completeData)

      return completeData
    })
    .then(({ complete, ...data }) => utils.encodeCODIA({ data }))
    .then(utils.setCache(cacheKey))
    .then(utils.sendData(res))
    .catch(utils.sendError(res))
})

app.get(
  '/models/:hashid/prices/',
  [
    middlewares.cache,
    middlewares.hashids,
    middlewares.mongodb({ projection: { 'prices._id': 0 }, returnProperty: 'prices' }),
  ],
  async (req, res) => {
    const {
      cacheKey,
      params: { codia },
    } = req

    axios.ia
      .get(`/models/${codia}/prices/`)
      .then(({ data }) => utils.setCache(cacheKey)(data))
      .then((prices) => utils.createOrUpdateModel(codia)({ prices }))
      .then(({ prices }) => utils.sendData(res)(prices))
      .catch(utils.sendError(res))
  }
)

app.get(
  '/models/:hashid/list_price',
  [
    middlewares.cache,
    middlewares.hashids,
    middlewares.mongodb({ projection: { _id: 0, list_price: 1 } }),
  ],
  async (req, res) => {
    const {
      cacheKey,
      params: { codia },
    } = req

    axios.ia
      .get(`/models/${codia}/list_price`)
      .then(({ data }) => utils.setCache(cacheKey)(data))
      .then(utils.createOrUpdateModel(codia))
      .then(utils.sendData(res))
      .catch(utils.sendError(res))
  }
)

app.get(
  '/models/:hashid/features/',
  [
    middlewares.cache,
    middlewares.hashids,
    middlewares.mongodb({ projection: { 'features._id': 0 }, returnProperty: 'features' }),
  ],
  async (req, res) => {
    const {
      cacheKey,
      params: { codia },
    } = req

    axios.ia
      .get(`/models/${codia}/features/`)
      .then(({ data }) => utils.setCache(cacheKey)(data))
      .then((features) => utils.createOrUpdateModel(codia)({ features }))
      .then(({ features }) => utils.sendData(res)(features))
      .catch(utils.sendError(res))
  }
)

// Swagger UI
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

const openapiSpec = swaggerJsdoc({
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'InfoAuto API',
      version: '1.0.0',
    },
    servers: [
      {
        url: `http://localhost:${process.env.EXPRESS_PORT}`,
        description: 'Development server',
      },
    ],
  },
  apis: ['./src/openapi.js'],
})
app.use('/swagger-ui', swaggerUi.serve, swaggerUi.setup(openapiSpec))

module.exports = app
