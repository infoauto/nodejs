const mongoose = require('mongoose')

module.exports = {
  connect: async () => {
    mongoose.set('strictQuery', true)
    return mongoose.connect(process.env.MONGODB_URI)
  },
  disconnect: mongoose.disconnect,
}
