const axios = require('axios')
const axiosRetry = require('axios-retry')

const auth = require('./auth')

const DEFAULT_HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'User-Agent': 'InfoAuto NodeJS',
}

const iaClient = axios.create({
  baseURL: process.env.BASE_URL + '/pub',
  headers: DEFAULT_HEADERS,
})

iaClient.interceptors.request.use(async (config) => {
  const token = await auth.getAccessToken()
  config.headers.Authorization = 'Bearer ' + token
  return config
})

const authClient = axios.create({
  baseURL: process.env.BASE_URL + '/auth',
  headers: DEFAULT_HEADERS,
})

//Taken from https://github.com/softonic/axios-retry/pull/208#discussion_r1027352021
function exponentialDelay(retryNumber = 0) {
  const randomMs = Math.random() * 500
  const exponentialDelay = Math.pow(2, retryNumber) * 500 + randomMs
  const maxBackoff = 60 * 1000
  const delay = Math.min(exponentialDelay, maxBackoff)
  const deadline = 5 * 60 * 1000
  if (delay > deadline) {
    throw new Error(
      `Exponential retry delay exhausted after ${
        deadline / 1000 / 60
      } minutes (${retryNumber} retries).`
    )
  }
  return delay
}

axiosRetry(iaClient, {
  retryCondition: (e) => {
    return (
      axiosRetry.isNetworkError(e) || axiosRetry.isRetryableError(e) || e.response.status === 429
    )
  },
  retryDelay: (retryCount, error) => {
    if (error.response && error.response.status === 429) {
      const retry_after = error.response.headers['retry-after']
      if (retry_after) {
        const randomMs = Math.random() * 250
        return retry_after * 1000 + randomMs
      }
    }
    return exponentialDelay(retryCount)
  },
})

axiosRetry(authClient, {
  retryCondition: (e) => {
    return axiosRetry.isNetworkError(e) || axiosRetry.isRetryableError(e)
  },
  retryDelay: exponentialDelay,
})

module.exports = {
  ia: iaClient,
  auth: authClient,
}
