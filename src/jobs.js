const mongoose = require('mongoose')

const redis = require('./redis')
const auth = require('./auth')
const cache = require('./cache')
const axios = require('./axios')
const models = require('./models')

const getBrands = async ({ session }) => {
  const brandsDetails = {},
    groupsDetails = {}

  const brandsResp = await axios.ia.get('/brands/download/')
  brandsResp.data.forEach(({ id: brandId, name, summary, groups }) => {
    brandsDetails[brandId] = { id: brandId, name, summary }
    groupsDetails[brandId] = {}
    groups.forEach(({ id: groupId, name, summary }) => {
      groupsDetails[brandId][groupId] = { id: groupId, name, summary }
    })
  })

  if (brandsResp.data.length) {
    await models.Brand.bulkWrite(
      brandsResp.data.map(({ id, ...rest }) => ({
        updateOne: {
          filter: { id },
          update: rest,
          upsert: true,
        },
      })),
      { session }
    )
  }

  return {
    brands: brandsDetails,
    groups: groupsDetails,
  }
}

const getFeatures = async ({ session }) => {
  const featuresDetails = {},
    choicesDescriptions = {}

  const featuresResp = await axios.ia.get('/features/')
  featuresResp.data.forEach(({ id, choices, ...rest }) => {
    featuresDetails[id] = { id, ...rest }
    if (rest.type === 'choice') {
      choicesDescriptions[id] = {}
      choices.forEach(({ description, long_description: value_description }) => {
        choicesDescriptions[id][description] = { value_description }
      })
    }
  })

  if (featuresResp.data.length) {
    await models.Feature.bulkWrite(
      featuresResp.data.map(({ id, ...rest }) => ({
        updateOne: {
          filter: { id },
          update: rest,
          upsert: true,
        },
      })),
      { session }
    )
  }

  return {
    features: featuresDetails,
    choices: choicesDescriptions,
  }
}

const getAuthenticatedUser = async () => {
  const token = await auth.getAccessToken()
  const resp = await axios.auth.get('/whoami', {
    headers: { Authorization: 'Bearer ' + token },
  })

  return resp.data
}

const comparePrices = (p1, p2) => {
  if (p1.year < p2.year) {
    return -1
  }
  return 1
}

const compareFeatures = (f1, f2) => {
  if (f1.category_name < f2.category_name) {
    return -1
  } else if (f1.category_name > f2.category_name) {
    return 1
  } else {
    if (f1.position < f2.position) {
      return -1
    }
    return 1
  }
}

const completeModelData =
  ({ brands, groups, features, choices }) =>
  ({ brand, group, features: featuresValues, prices, ...rest }) => {
    return {
      ...rest,
      complete: true,
      brand: brands[brand.id],
      group: groups[brand.id][group.id],
      prices: prices.sort(comparePrices),
      features: featuresValues
        .map(({ id, value }) => {
          const feature = features[id]
          const featureValue = { ...feature, value }

          return feature.type !== 'choice'
            ? featureValue
            : { ...featureValue, ...choices[id][value] }
        })
        .sort(compareFeatures),
    }
  }

const bulkReplacement = async ({ batchSize, session }) => {
  let start = 0,
    query = [],
    updatedModels = [],
    badModels = []

  const { brands, groups } = await getBrands({ session })
  const { features, choices } = await getFeatures({ session })

  const user = await getAuthenticatedUser()
  if (user.roles.find((role) => role.name === 'Lotes') === undefined) {
    return models.Model.deleteMany({}, { session })
  }

  do {
    query = await models.Model.find(
      {},
      { codia: 1, _id: 0 },
      { skip: start, limit: batchSize }
    ).lean()

    if (query.length) {
      const batch = query.map(({ codia }) => codia)
      axios.ia
        .post('/batch/', { batch })
        .then(({ data }) => {
          const batchModels = data.map(completeModelData({ brands, groups, features, choices }))
          updatedModels = [...updatedModels, ...batchModels]
        })
        .catch(() => {
          badModels = [...badModels, ...batch]
        })

      await new Promise((resolve) => setTimeout(resolve, 1200))
    }
    start += batchSize
  } while (query.length)

  if (updatedModels.length || badModels.length) {
    const updates = updatedModels.map(({ codia, ...rest }) => ({
      updateOne: { filter: { codia }, update: rest },
    }))

    const replacements = badModels.map((codia) => ({
      replaceOne: { filter: { codia }, replacement: { codia, rejected: true } },
    }))

    return models.Model.bulkWrite(updates.concat(replacements), { session })
  } else {
    return Promise.resolve()
  }
}

const updateData = async ({ batchSize = 100 } = {}) => {
  const lastUpdate = await redis.get('updated_at')
  return axios.ia.get('/datetime').then(async ({ data: { updated_at } }) => {
    if (lastUpdate !== updated_at) {
      const session = await mongoose.startSession()
      return session
        .withTransaction(async () => {
          return bulkReplacement({ batchSize, session }).then(async (res) => {
            await cache.clear()
            await redis.set('updated_at', updated_at)
            return Promise.resolve(res)
          })
        })
        .then(() => session.endSession())
    }
  })
}

const login = async () =>
  axios.auth
    .request({
      url: '/login',
      method: 'post',
      auth: {
        username: process.env.USERNAME,
        password: process.env.PASSWORD,
      },
    })
    .then(({ data: { access_token, refresh_token } }) =>
      auth.setTokens(access_token, refresh_token)
    )
    .catch(auth.deleteTokens)

const refresh = async () => {
  const token = await auth.getRefreshToken()
  if (!token) {
    return login()
  }

  return axios.auth
    .request({
      url: '/refresh',
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    .then(({ data: { access_token } }) => auth.setTokens(access_token))
    .catch(login)
}

module.exports = {
  login,
  refresh,
  updateData,
}
