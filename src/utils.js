const cache = require('./cache')
const axios = require('./axios')
const hashids = require('./hashids')
const models = require('./models')

const setPagination = (res) => (response) => {
  const paginationMetadata = response.headers['x-pagination']
  if (paginationMetadata !== undefined) {
    res.set('X-Pagination', paginationMetadata)
  }
  return response
}

const setCache = (key) => async (data) => {
  await cache.set(key, data)
  return data
}

const encodeCODIA = ({ data }) => hashids.codiaEncoder(data)

const sendError = (res) => (error) => {
  res.status(error.response.status).send(error.response.data)
}

const sendData = (res) => (data) => res.send(data)

const createOrUpdateModel = (codia) => async (data) => {
  try {
    await models.Model.create({ codia, ...data })
  } catch (error) {
    if (error.code === 11000) {
      await models.Model.updateOne({ codia }, data)
    } else throw error
  }
  return data
}

const completeModelData = async (data) => {
  const { codia, as_codia, list_price } = data
  let listPriceReq, asCodiaReq

  if (list_price) {
    listPriceReq = axios.ia.get(`/models/${codia}/list_price`)
  } else {
    listPriceReq = new Promise((resolve) => resolve({ data: { list_price: null } }))
  }
  if (as_codia) {
    asCodiaReq = axios.ia.get(`/models/${codia}/as_codia`)
  } else {
    asCodiaReq = new Promise((resolve) => resolve({ data: { codia: null } }))
  }
  const pricesReq = axios.ia.get(`/models/${codia}/prices/`)
  const featuresReq = axios.ia.get(`/models/${codia}/features/`)

  const [listPriceResp, pricesResp, featuresResp, asCodiaResp] = await Promise.all([
    listPriceReq,
    pricesReq,
    featuresReq,
    asCodiaReq,
  ])

  data.list_price = listPriceResp.data.list_price
  data.prices = pricesResp.data
  data.features = featuresResp.data
  data.as_codia = asCodiaResp.data.codia
  data.complete = true

  delete data.prices_from
  delete data.prices_to

  return data
}

const completeAssimilatedModelData = async (data) => {
  const { codia: as_codia } = data

  const listPriceReq = axios.ia.get(`/models/${as_codia}/list_price`)
  const pricesReq = axios.ia.get(`/models/${as_codia}/prices/`)
  const featuresReq = axios.ia.get(`/models/${as_codia}/features/`)

  const [listPriceResp, pricesResp, featuresResp] = await Promise.all([
    listPriceReq,
    pricesReq,
    featuresReq,
  ])

  data.as_codia = null
  data.list_price = listPriceResp.data.list_price
  data.prices = pricesResp.data
  data.features = featuresResp.data
  data.complete = true

  delete data.prices_from
  delete data.prices_to

  return data
}

module.exports = {
  setPagination,
  setCache,
  encodeCODIA,
  sendData,
  sendError,
  createOrUpdateModel,
  completeModelData,
  completeAssimilatedModelData,
}
