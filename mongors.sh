#!/bin/bash

sleep 5

echo "Starting replica set initialize"
until mongosh --host mongodb1 --eval "print(\"waited for connection\")"
do
    sleep 2
done
echo "Connection finished"
echo "Creating replica set"
mongosh --host mongodb1 <<EOF
rs.initiate(
    {
    _id : 'mongors',
    members: [
        { _id : 0, host : "mongodb1" },
        { _id : 1, host : "mongodb2" }
    ]
    }
)
EOF
echo "Replica set created"

sleep 10
mongosh --host mongodb1 --eval "rs.status()"