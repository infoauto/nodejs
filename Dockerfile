FROM node:18-bullseye-slim

WORKDIR /app

COPY package*.json ./

RUN npm ci --omit=dev

COPY --chown=node:node . ./

USER node

CMD [ "npm", "run" ]